#include <stdexcept>
#include <iostream>
#include <string>
#include "pipe.h"


Element::Element(const int aX, const int aY, TypeOfPipe aType) {
    _x = aX;
    _y = aY;
    type = aType;
}

Element::~Element() {
    delete(this);
}

bool Element::IsConnectedCorrectly(const IPipe* aPipe) const {
    if(this->getType() == east_west){
        if(this -> _y == 0 || this ->_y == aPipe -> getSize() -1)
            return  false;

        TypeOfPipe leftSide = aPipe->getElement(_x, _y-1)->getType();
        TypeOfPipe rightSide = aPipe->getElement(_x,_y+1)->getType();

        if(leftSide == nothing ||rightSide == nothing)
            return false;
    }

    if(this->getType() == north_south){
        if(this -> _x == 0 || this ->_x == aPipe -> getSize() -1)
            return  false;

        TypeOfPipe topSide = aPipe->getElement(_x-1, _y)->getType();
        TypeOfPipe bottomSide = aPipe->getElement(_x+1,_y)->getType();

        if(topSide == nothing || bottomSide == nothing)
            return false;
    }

    if(this->getType() == north_south_east_west){
        if(this -> _y == 0 || this ->_y == aPipe -> getSize() -1 || this->_x == 0 || this->_x == aPipe->getSize() - 1)
            return  false;

        TypeOfPipe leftSide = aPipe->getElement(_x, _y-1)->getType();
        TypeOfPipe rightSide = aPipe->getElement(_x,_y+1)->getType();
        TypeOfPipe topSide = aPipe->getElement(_x-1, _y)->getType();
        TypeOfPipe bottomSide = aPipe->getElement(_x+1,_y)->getType();

        if(topSide == nothing || bottomSide == nothing || leftSide == nothing || rightSide == nothing)
            return false;
    }

    if(this->getType() == south_east_west){
        if(this -> _y == 0 || this ->_y == aPipe -> getSize() -1 || this->_x == aPipe->getSize() - 1)
            return  false;

        TypeOfPipe leftSide = aPipe->getElement(_x, _y-1)->getType();
        TypeOfPipe rightSide = aPipe->getElement(_x,_y+1)->getType();
        TypeOfPipe bottomSide = aPipe->getElement(_x+1,_y)->getType();

        if(bottomSide == nothing || leftSide == nothing || rightSide == nothing)
            return false;
    }
    return  true;
}

const TypeOfPipe Element::getType() const
{
    return type;
}

Pipe::~Pipe()
{
}

Pipe::Pipe(const int aX) {
    size = aX;
    for (size_t i = 0; i < aX; i++) {
        matrix.insert(matrix.begin(), vector<APipeElement*>());
    }
}

void Pipe::addElement(const int aX, const int aY, APipeElement *aElement) {
    matrix.at(aX).insert(matrix.at(aX).end(), aElement);
}

const APipeElement* Pipe::getElement(int aX, int aY) const {
    if(matrix.size() < aX || matrix.at(aX).size() < aY || aX < 0 || aY < 0){
            throw out_of_range("Error! Out of range.");
    } else {
        return matrix.at(aX).at(aY);
    }
}

const int Pipe::getSize() const {
    return size;
}

bool Pipe::IsConnectedCorrectly() const {
    for (int i = 0; i < size ; ++i) {
        vector<APipeElement *> row = matrix.at(i);
        for (int j = 0; j < size; ++j) {
            if(row.at(j) -> IsConnectedCorrectly(this) == false)
            {
                return false;
            }
        }
    }
}

ifstream& operator >> (ifstream& aInput,Pipe& aA){
    char direction = 0;
    APipeElement* newElement = nullptr;
    int xNext = -1;
    string line;


    while(getline(aInput, line)) {
        int i = 0;
        ++xNext;
        int yNext = 0;

        for (int i = 0; i < line.length(); i++) {
            direction = line.at(i);
            switch (direction) {
                case '-':
                    newElement = new Element(xNext, yNext, east_west);
                    break;
                case 'I':
                    newElement = new Element(xNext, yNext, north_south);
                    break;
                case '+':
                    newElement = new Element(xNext, yNext, north_south_east_west);
                    break;
                case 'O':
                    newElement = new Element(xNext, yNext, cap);
                    break;
                case 'T':
                    newElement = new Element(xNext, yNext, south_east_west);
                    break;
                case ' ':
                    newElement = new Element(xNext, yNext, nothing);
                    break;
            }
            aA.addElement(xNext, yNext++, newElement);
        }

        for (; i < aA.getSize(); i++)
        {
            newElement = new Element(xNext, yNext, nothing);
            aA.addElement(xNext, yNext++, newElement);
        }


    }
    return aInput;
}

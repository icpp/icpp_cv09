#include <vector>
#include <iosfwd>
#include <iostream>


using namespace std;

enum TypeOfPipe {
    //west - západ
    //east - východ
    //north - sever
    //south - jih
   east_west,
   north_south,
   north_south_east_west,
   nothing, //Nic
   cap, //Uzávěr
   south_east_west
};

struct APipeElement; //APotrubniPrvek
struct IPipe {
    virtual ~IPipe() { }
    virtual const APipeElement* getElement(int x, int y) const = 0; //DejPrvek
    virtual bool IsConnectedCorrectly() const = 0; //JePotrubiOk
    virtual const int getSize() const = 0;
};

struct APipeElement {
    virtual ~APipeElement() { }
    virtual bool IsConnectedCorrectly(const IPipe* aPipe) const = 0; // JePotrubiOk
    virtual const TypeOfPipe getType() const = 0;
    int _x;
    int _y;
};


class Pipe: public IPipe {
    private:
        vector<vector<APipeElement*>> matrix = vector<vector<APipeElement*>>();
        int size = 0;
    public:
        Pipe(const int aX);
        ~Pipe();
        void addElement(const int aX, const int aY, APipeElement * aElement);
        const APipeElement* getElement(int aX, int aY) const;
        bool IsConnectedCorrectly() const;
        const int getSize() const;
};

ifstream& operator >> (ifstream& output, Pipe& a);

class Element : public APipeElement {
    private:
        TypeOfPipe type;
    public:
        Element(const int aX, const int aY, TypeOfPipe aType);
        ~Element();
        bool IsConnectedCorrectly(const IPipe* aPipe) const;
        const TypeOfPipe getType() const;
};



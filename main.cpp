#include <iostream>
#include <fstream>
#include <string>
#include "pipe.h"
using namespace std;

int main()
{
    ifstream file("a.txt", ifstream::in);

    string line;
    getline(file, line);
    int size = stoi(line);
    Pipe p = Pipe(size);
    file >> p;
    cout << p.IsConnectedCorrectly() << endl;

    file = ifstream("b.txt", ifstream::in);


    getline(file, line);
    size = stoi(line);
    p = Pipe(size);
    file >> p;
    cout << p.IsConnectedCorrectly() << endl;

    file = ifstream("c.txt", ifstream::in);

    getline(file, line);
    size = stoi(line);
    p = Pipe(size);
    file >> p;
    cout << p.IsConnectedCorrectly() << endl;

    file = ifstream("d.txt", ifstream::in);

    getline(file, line);
    size = stoi(line);
    p = Pipe(size);
    file >> p;
    cout << p.IsConnectedCorrectly() << endl;

    file = ifstream("e.txt", ifstream::in);

    getline(file, line);
    size = stoi(line);
    p = Pipe(size);
    file >> p;
    cout << p.IsConnectedCorrectly() << endl;

    file = ifstream("f.txt", ifstream::in);

    getline(file, line);
    size = stoi(line);
    p = Pipe(size);
    file >> p;
    cout << p.IsConnectedCorrectly() << endl;

    file = ifstream("g.txt", ifstream::in);

    getline(file, line);
    size = stoi(line);
    p = Pipe(size);
    file >> p;
    cout << p.IsConnectedCorrectly() << endl;

    file = ifstream("h.txt", ifstream::in);

    getline(file, line);
    size = stoi(line);
    p = Pipe(size);
    file >> p;
    cout << p.IsConnectedCorrectly() << endl;

    return 0;

} 